package com.wissen.training.nisarga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoarGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoarGatewayApplication.class, args);
	}

}
